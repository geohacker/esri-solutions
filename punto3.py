import arcpy
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "C:/data"

# Set local variables
inRaster = "Hansen_ZRC"
reclassField = "year"
remap = RemapValue([["2001", 1], ["2002", 2],["2003", 3],["2004", 4],["2005", 5],["2006", 6],["2007", 7],["2008", 8],["2009", 9]])

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Execute Reclassify
outReclassify = Reclassify(inRaster, reclassField, remap, "NODATA")

# Save the output 
outReclassify.save("C:/resultados/output/outreclass")